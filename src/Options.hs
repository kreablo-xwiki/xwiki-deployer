module Options (
  Options(..),
  getOptions
) where


import qualified Options.Applicative as Opts
import Prelude                    ((.), String, Bool(..))
import System.IO                  (IO)
import Data.Maybe
import Data.Text                  (Text, pack)
import System.FilePath
import Data.Functor
import Control.Applicative
import Data.Monoid

data Options = Options {
  optItemFile :: String,
  optWiki :: Maybe Text,
  optListSpaces :: Bool,
  optListUpdated :: Bool,
  optSyncUpdated :: Bool,
  optPullXar :: Bool,
  optPullBackup :: Bool,
  optCreateNonexisting :: Bool,
  optCacheDir :: Maybe FilePath
  }

getOptions :: IO Options
getOptions = Opts.execParser optInfo
  where
    optInfo = Opts.info (Opts.helper <*> options) (Opts.fullDesc <> Opts.progDesc "Deploy a set of files corresponding to contents in XWiki entites on a Wiki")
    options = Options <$> Opts.strOption (Opts.long "project-json" <> Opts.metavar "JSON-FILE" <> Opts.help "The name of the project JSON file.")
                      <*> Opts.option (Just . pack <$> Opts.str) (Opts.long "wiki" <> Opts.metavar "WIKINAME" <> Opts.value Nothing <> Opts.help "The database name of the wiki.")
                      <*> Opts.switch (Opts.long "list-spaces" <> Opts.help "List toplevel spaces of wiki.")
                      <*> Opts.switch (Opts.long "list-updated" <> Opts.help "Only list what pages are updated.")
                      <*> Opts.switch (Opts.long "sync-updated" <> Opts.help "Fetch updated remote entities and write to local files.")
                      <*> Opts.switch (Opts.long "pull-xar" <> Opts.help "Pull a xar-file")
                      <*> Opts.switch (Opts.long "pull-backup" <> Opts.help "Pull a xar-file")
                      <*> Opts.switch (Opts.long "create" <> Opts.help "Create non-existing entities.")
                      <*> Opts.option (Just <$> Opts.str) (Opts.long "cache-dir" <> Opts.help "Cache dir for storing entities to detect conflicts.")
