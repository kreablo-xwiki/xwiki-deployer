{-# LANGUAGE TupleSections #-}
module XWiki.EntityCache (
  getCachedEntity  
) where

import XWiki.Model
import XWiki.Rest.Class
import Data.Aeson (FromJSON, eitherDecode)
import Prelude
import Options
import Data.Text (Text)
import System.FilePath
import System.Posix.Files
import GHC.IO hiding (liftIO)
import Control.Monad.Trans
import qualified Data.ByteString.Lazy as BL

getCachedEntity :: (FromJSON a, XWikiRest r, MonadIO r) => Options -> Maybe Text -> EntityReference -> r (Either String (Maybe (Bool, a)))
getCachedEntity opts translation entityRef = do
  cached <- case optCacheDir opts of
    Just cacheDir -> liftIO $ catchAny (
      do
        let path = foldl (</>) cacheDir $ entityFilepath entityRef
        let dir = dropFileName path

        exists <- fileExist path
        dirExists <- fileExist dir
        dirFs <- getFileStatus dir

        if (exists)
          then (do
                   fmap (Just . (True,)) . eitherDecode <$> BL.readFile path
               )
          else pure $ Right Nothing
      ) (\e -> pure $ Left ("Failed to get cached entity: " <> show e))

    Nothing -> pure $ Right Nothing

  case cached of
    Right (Just _) -> pure cached
    Left _         -> pure cached
    Right Nothing  -> do
      result <- getEntity translation entityRef
       (fmap (False,)) <$> 
  
