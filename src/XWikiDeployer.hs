{-# LANGUAGE OverloadedStrings #-}
module Main(main)
where

import Prelude                    (null, (.), ($), (/=), Bool(..), (||), filter, flip, (&&), (==), not, print, show, String)
import Options
import System.IO                  (IO)
import System.Posix.Directory     (changeWorkingDirectory)
import Control.Applicative
import qualified Data.ByteString.Lazy as BL      (readFile, writeFile)
import Data.Either
import Data.Maybe
import Data.Text                  (Text, pack, unpack, split)
import Data.Monoid
import Data.Bifunctor
import qualified Data.Text.IO as TextIO          (readFile, putStrLn, writeFile)
import Control.Monad
import Control.Monad.Except
import Data.Aeson                 (eitherDecode)
import System.FilePath
import XWiki.Rest.Curl
import XWiki.Rest.Class
import XWiki.Wiki
import XWiki.Rest.Space
import XWiki.Rest.Format
import XWiki.Model
-- import XWiki.EntityCache
import Data.Algorithm.Diff as Diff
import Data.Algorithm.DiffOutput as Diff
import Data.Foldable

main :: IO ()
main = do
  opts <- getOptions
  let dir = dropFileName $ optItemFile opts
  unless (null dir) $ changeWorkingDirectory dir
  wikiItem' <- eitherError $ eitherDecode <$> BL.readFile (takeFileName $ optItemFile opts)
  let wiki = optWiki opts <|> xwikiWiki wikiItem'
  let wikiItem = wikiItem' { xwikiWiki = wiki }
  withXWikiRestDo (case (optListSpaces opts, optPullXar opts || optPullBackup opts) of
                     (True, _) -> listSpaces wikiItem
                     (_, True) -> pullXar wikiItem (optPullBackup opts)
                     _         -> updateEntities opts $ xwikiEntities wikiItem) wikiItem
  pure ()

topLevelSpace :: Text -> Bool
topLevelSpace s =
  case parseEntity s of
    Right (DocumentEntity (DocumentReference (SpaceReference _ []) _)) -> True
    _ -> False

listSpaces' :: XWikiRest r => WikiItem -> r [Text]
listSpaces' wikiItem =
  case xwikiWiki wikiItem of
    Nothing -> fail "No wiki parameter"
    Just w -> do
      espaces <- getSpaceEntities $ Left (WikiReference w)
      case espaces of
        Left error -> fail error
        Right spaces -> pure $ filter topLevelSpace $ xwsId <$> spaces

listSpaces :: XWikiRest r => WikiItem -> r ()
listSpaces wikiItem = listSpaces' wikiItem >>= mapM_ (liftIO . TextIO.putStrLn)

pullXar :: XWikiRest r => WikiItem -> Bool -> r ()
pullXar wikiItem pullBackup =
  listSpaces' wikiItem >>= flip pullXarWithSpaces pullBackup
  
updateEntities :: XWikiRest r => Options -> [Entity] -> r ()
updateEntities opts = mapM_ (updateEntity opts)


updateEntity :: XWikiRest r => Options -> Entity -> r ()
updateEntity opts entity@(Entity _ file (AttachmentEntity _)) =
  updateEntity'
      (BL.readFile file)
      (BL.writeFile file)
      getEntityContent
      (((.).(.).(.)) void putEntityContent)
      diff
      OCTET
      opts
      entity

  where
    diff (Just a) b = if a /= b then Just "The file content differs." else Nothing
    diff _ ""       = Nothing
    diff _ _        = Just "No entity but file content is non-empty."  
      
updateEntity opts entity@(Entity translation file entityRef) =
  updateEntity'
    (TextIO.readFile file)
    (TextIO.writeFile file)
    (get)
    (((.).(.).(.)) void putEntity)
    diff
    JSON
    opts
    entity
  where
    diff (Just ec) fc =
      let elines = filter (/= mempty) (split (\c -> c == '\n' || c == '\r') ec)
          flines = filter (/= mempty) (split (\c -> c == '\n' || c == '\r') fc)
          d      = Diff.ppDiff (getGroupedDiff (unpack <$> elines) (unpack <$> flines))
      in
        if flines /= elines
        then Just $ fold $ fold (maybeToList <$> [(translation), Just (pack $ show entityRef), Just (pack d)])
        else Nothing

    diff _ ""       = Nothing
    diff _ _        = Just "No entity but file content is non-empty."

    get translation' entityRef' = do
      
      result <- getEntity translation' entityRef'
      pure $ (>>= mapM (first unpack . entityContent entityRef')) result
        

{- do
  setFormat JSON
                      
  -- result <- eitherError $ getCachedEntity opts translation entityRef

  result <- eitherError $ getEntity translation entityRef

  createNonExisting $ optCreateNonexisting opts

  when (isNothing result && not (optCreateNonexisting opts)) $ fail ("Entity does not exist: " <> show entityRef <> ", try --create")
    
  ec <- maybe (pure "") (pure . entityContent entityRef . snd) result
  fc <- liftIO $ TextIO.readFile file
  let elines = filter (/= mempty) (split (\c -> c == '\n' || c == '\r') ec)
  let flines = filter (/= mempty) (split (\c -> c == '\n' || c == '\r') fc)
  when (elines /= flines && optSyncUpdated opts && isJust result) $ liftIO $ TextIO.writeFile file ec
  when (elines /= flines && not (optListUpdated opts)) $ void $ putEntity fc translation entityRef
  when (elines /= flines && optListUpdated opts) $ liftIO $ when (isJust translation) (print translation) >> print entityRef  >> putStrLn (Diff.ppDiff $ getGroupedDiff (unpack <$> elines) (unpack <$> flines))
-}

updateEntity' :: XWikiRest r =>
  (IO c) ->
  (c -> IO ()) ->
  ((Maybe Text) -> EntityReference -> r (Either String (Maybe c))) ->
  (c -> Maybe Text -> EntityReference -> r ()) ->
  (Maybe c -> c -> Maybe Text) ->
  Format ->
  Options ->
  Entity ->
  r ()
updateEntity' getFC putFC getEntity' putEntity' diff format opts  (Entity translation _ entityRef) = do
  setFormat format

  result <- eitherError $ getEntity' translation entityRef

  createNonExisting $ optCreateNonexisting opts

  when (isNothing result && not (optCreateNonexisting opts)) $ fail ("Entity does not exist: " <> show entityRef <> ", try --create")

  fc <- liftIO $ getFC
  
  case diff result fc of
    Just diff' -> do
      when (optSyncUpdated opts)       $ liftIO $ mapM_ putFC result
      when (not (optListUpdated opts)) $ putEntity' fc translation entityRef
      when (optListUpdated opts)       $ liftIO $ do
        when (isJust translation) (print translation)
        print entityRef
        TextIO.putStrLn diff'
    Nothing  -> pure ()
